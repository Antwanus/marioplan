import firebase from "firebase/app";
import 'firebase/firestore';
import 'firebase/auth';

var firebaseConfig = {
    apiKey: "AIzaSyCkroiZXpwCVmzjvwYKDD3xnzSSqvQ2B5A",
    authDomain: "twanus-marioplan.firebaseapp.com",
    databaseURL: "https://twanus-marioplan.firebaseio.com",
    projectId: "twanus-marioplan",
    storageBucket: "twanus-marioplan.appspot.com",
    messagingSenderId: "491731822168",
    appId: "1:491731822168:web:1d7eb1db22601d564acc2d",
    measurementId: "G-C55TKXFPKM"
  };

  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  firebase.firestore();

  export default firebase;