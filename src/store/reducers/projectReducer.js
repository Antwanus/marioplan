const initState = {
    projects: [
        {id: '1', title: 'Bibbi', content:'bibbibibbibibbibibbibibbibibbi'},
        {id: '2', title: 'babs', content: 'babsbabsbabsbabsbabsbabsbabsbabs'},
        {id: '3', title: 'bip', content: 'bipbipbipbipbipbipbipbipbipbipbipbip'}
    ]
};

const projectReducer = (state = initState, action) => {
    switch (action.type) {
        case 'CREATE_PROJECT':
            console.log('Created project', action.project);
            return state;
            case 'CREATE_PROJECT_ERROR':
                console.log('Error creating project', action.err);
            return state;
        default:
            return state;
    }
}

export default projectReducer;