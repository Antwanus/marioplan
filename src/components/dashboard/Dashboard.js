import React, { Component } from "react";
import Notifications from "./Notification";
import ProjectList from "../project/ProjectList";
import { connect } from "react-redux";
import { firestoreConnect } from "react-redux-firebase";
import { compose } from "redux";
import { polyfill } from "react-lifecycles-compat";
import { Redirect } from "react-router-dom";

class Dashboard extends Component {
    render(){
        const { projects, auth } = this.props;
        
        if( !auth.uid ) return <Redirect to='/signin' />

        return(
            <div className="dashboard container">
                <div className="row">
                    <div className="col s12 m6">
                        <ProjectList projects={ projects }></ProjectList>
                    </div>
                    <div className="col s12 m5 offset-m1">
                        <Notifications></Notifications>
                    </div>
                </div>
            </div>
        )
    }
}
polyfill(Dashboard);

const mapStateToProps = (state) => {
    return {
        projects: state.firestore.ordered.projects,
        auth: state.firebase.auth
    }
}

export default compose(
    connect(mapStateToProps),
    firestoreConnect([{ collection: 'projects'}])
    )(Dashboard);